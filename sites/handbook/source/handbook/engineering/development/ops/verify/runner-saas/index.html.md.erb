---
layout: handbook-page-toc
title: "Verify:Runner SaaS"
description: "The GitLab Runner SaaS Group's team page."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

By 2025, our vision for GitLab Runner SaaS is that it is the most performant and reliable cloud-hosted CI build platform.

## Mission

Our mission is to enable organizations to run most of their  GitLab CI/CD jobs on GitLab SaaS. This means developing solutions that meet the most stringent compliance requirements and provides methods for securely connecting the GitLab SaaS Runner platform with a customer infrastructure.

This team maps to [Verify](/handbook/product/categories/#verify-stage) devops stage.

## Product Strategy and Roadmap

The vision and strategy for the Runner SaaS group is covered on the [Runner SaaS](/direction/verify/runner_saas) page.

## Performance Indicator

In the [OPS section](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/), we continuously define, measure, analyze, and iterate or Performance Indicators (PIs). One of the PI process goals is to ensure that, as a product team, we are focused on strategic and operational improvements to improve leading indicators, precursors of future success.

## Team Members

The following people are permanent members of the Verify:Runner SaaS group:

<%= shared_team_members(role_regexps: [/Verify:Runner SaaS/i]) %>

## Stable Counterparts

To find out who our stable counterparts are, look at the [runner product
categtory](/handbook/product/categories/#runner-group)

## Dashboards

<%= partial "handbook/engineering/metrics/partials/_cross_functional_dashboard.erb", locals: { filter_value: "Runner" } %>


## Projects we maintain

As a team we maintain several projects. The <https://gitlab.com/gitlab-com/runner-maintainers> group
is added to each project with maintainer permission. We also try to align tools and versions used across them.

- [GCP Windows Shared Runners base VM image](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers)
- [macOS VM images](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka)

## Development Process

### Iterations

The Gitlab Runner team works in monthly iterations. While the feature freeze for the core runner project is on the 7th in preparation for the release on the 22nd of each month, iteration planning dates for the upcoming milestone are aligned with GitLab's [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline).

At a minimum, 30 days before the start of a milestone, the runner PM reviews and re-prioritizes as needed the features to be included for developer assignment in the iteration. Note - this is and must be a conversation with the PM, EM and members of the team.

The commitments for the iteration plan are directly related to the capacity of the team for the upcoming iteration. Therefore, to finalize the iteration plan (resource allocation) for a milestone, we evaluate and consider the following:

- Forced prioritization issues (these issues will always be first in line for resource allocation.)
- In flight development work that did not complete prior to the feature freeze.
- Strategic direction features.
- Community or customer requested features.
- Bugs
- Technical Debt
- Maintenance
- Community merge requests review assignments

### Iteration Planning and Issue Refinement Process

1. The PM creates iteration planning issues for at minimum the next three milestones.
1. The PM adds candidate issues to the planning issues.
1. The PM adds the scoped label `~candidate::x.y` to each issue. For example `~candidate::16.0`
1. The PM assigns the iteration planning issues to the runner engineering manager (EM).
1. The EM reviews all candidate tech debt, bugs, security and feature issues and validates if the `~workflow::ready for development` label can be applied or the issue needs further refinement. The priority is the next in queue milestone.
1. The EM will assign issues needing refinement to individual engineers and add the `~workflow::refinement` label to the issue.
1. **Issue Refinement**: engineers assigned to refine an issue are responsible for coming up with an implementation plan for the issue - exploring any unknowns, edge cases, or compatibility challenges that may come up and proposing how the issue can be broken down into smaller tasks that can be iterated on quickly. We strive to iterate and break down the efforts to ship as much value in the milestone for our users as possible. If you see a more efficient way forward when you start working on issue refinement, you are free to update the proposal and create a new issue to deliver value more quickly to our users.
1. At minimum, three business days prior to GitLab's monthly release [kickoff](https://about.gitlab.com/handbook/engineering/workflow/#kickoff) livestream, the PM, EM, Quality and UX leads finalize the iteration plan for the upcoming milestone. All issues that are targeted for delivery in the milestone must have an engineer assigned.
1. Finally the EM adds the `~deliverable` label to all issues and removes the scoped label `~candidate::x.y` from the issue.

## Issue Health Status Definitions

- **On Track** - We are confident this issue will be completed and live for the current milestone. It is all [downhill from here](https://basecamp.com/shapeup/3.4-chapter-12#work-is-like-a-hill).
- **Needs Attention** - There are concerns, new complexity, or unanswered questions that if left unattended will result in the issue missing its targeted release. Collaboration needed to get back `On Track` within the week.
   - If you are moving an item into this status please mention individuals in the issue you believe can help out in order to unstick the item so that it can get back to an `On Track` status.
- **At Risk** - The issue in its current state will not make the planned release and immediate action is needed to get it back to `On Track` today.
  - If you are moving an item into this status please consider posting in a relevant team channel in slack. Try to include anything that can be done to unstick the item so that it can get back to an `On Track` status in your message.
  - Note: It is possible that there is nothing to be done that can get the item back on track in the current milestone. If that is the case please let your manager know as soon as you are aware of this.

## Async Issue progress updates

When an engineer is actively working (workflow of ~workflow::"In dev" or further right on current milestone) on an issue they will periodically leave status updates as top-level comments in the issue. The status comment should include the updated health status, any blockers, notes on what was done, if review has started, and anything else the engineer feels is beneficial. If there are multiple people working on it also include whether this is a front end or back end update. An update for each of MR associated with the issue should be included in the update comment. Engineers should also update the [health status](https://docs.gitlab.com/ee/user/project/issues/#health-status) of the issue at this time.

This update need not adhere to a particular format. Some ideas for formats:

```markdown
Health status: (On track|Needs attention|At risk)
Notes: (Share what needs to be shared specially when the issue needs attention or is at risk)
```

```markdown
Health status: (On track|Needs attention|At risk)
What's left to be done:
What's blocking: (probably empty when on track)
```

```markdown
## Update <date>
Health status: (On track|Needs attention|At risk)
What's left to be done:

#### MRs
1. !MyMR1
1. !MyMR2
1. !MyMR3
```
There are several benefits to this approach:

* Team members can better identify what they can do to help the issue move along the board
* Creates an opening for other engineers to engage and collaborate if they have ideas
* Leaving a status update is a good prompt to ask questions and start a discussion
* The wider GitLab community can more easily follow along with product development
* A history of the roadblocks the issue encountered is readily available in case of retrospection
* Product and Engineering managers are more easily able to keep informed of the progress of work

Some notes/suggestions:

* We typically expect engineers to leave at least one status update per week, barring special circumstances
* Ideally status updates are made at a logical part of an engineers workflow, to minimize disruption
* It is not necessary that the updates happen at the same time/day each week
* Generally when there is a logical time to leave an update, that is the best time
* Engineers are encouraged to use these updates as a place to collect some technical notes and thoughts or "think out loud" as they work through an issue.

## How to work with us

### On issues

Issues worked on by the Runner group a group label of `~group::runner saas`. Issues that contribute to the verify stage of the devops toolchain have the `~devops::verify` label.

### Get our attention

GitLab.com: `@gitlab-com/runner-group`
